[![Build Status](https://travis-ci.org/dpopkov/job4j.svg?branch=master)](https://travis-ci.org/dpopkov/job4j)
[![codecov](https://codecov.io/gh/dpopkov/job4j/branch/master/graph/badge.svg)](https://codecov.io/gh/dpopkov/job4j)

# Репозиторий Дениса Попкова.

Начал обучение по курсу [Job4j](http://job4j.ru). Планирую завершить его к маю 2019 года.

Ниже перечислены проекты, реализованые мной во время обучения.
Список пока пустой, проекты будут добавляться по мере реализации.


#### VM options для запуска проектов JavaFX на JDK 11

--module-path c:\path-to-javafx\javafx-sdk-11\lib  
--add-modules=javafx.controls,javafx.fxml

В курсе работа производилась в ветках git.

Объединение зафиксированных изменений.

Создание новой ветки через IDEA.
